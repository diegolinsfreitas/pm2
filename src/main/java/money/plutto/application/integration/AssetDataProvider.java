package money.plutto.application.integration;

import money.plutto.application.model.Asset;

import java.io.IOException;
import java.util.Collection;
import java.util.List;


public interface AssetDataProvider {

    String providerID();

    //TODO https://stackoverflow.com/questions/41076500/eventlistener-for-authenticationsuccessevent-or-interactiveauthenticationsucces https://dzone.com/articles/spring-boot-creating-asynchronous-methods-using-as only update assets from logged users
    void updateAssetsData(List<String> assetsCodes) throws IOException;

    Collection<? extends Asset> queryAsset(String criteria);

    String retrieveCleanAssetTicker(String ticker);
}
