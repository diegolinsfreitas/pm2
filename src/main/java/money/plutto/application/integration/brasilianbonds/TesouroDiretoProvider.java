package money.plutto.application.integration.brasilianbonds;

import money.plutto.application.integration.AssetDataProvider;
import money.plutto.application.model.Asset;
import money.plutto.application.repository.AssetRepository;
import lombok.AllArgsConstructor;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class TesouroDiretoProvider implements AssetDataProvider {

    private final AssetRepository assetRepository;

    @Override
    public String providerID() {
        return "TESOURO";
    }

    @Override
    public void updateAssetsData(List<String> assetsCodes) throws IOException {

        List<Asset> assets = assetRepository.findAssetsBySymbolIsIn(assetsCodes.toArray(String[]::new));
        //Call service api
        assets.forEach(asset -> {
            asset.setLastPrice(Money.of(600, "BRL"));
            assetRepository.save(asset);
        });
    }

    @Override
    public Collection<? extends Asset> queryAsset(String criteria) {
        Asset asset = Asset.builder()
                .name("Tesouro Selic")
                .symbol("Tesouro Selic")
                .originalSymbol("Tesouro Selic")
                .lastPrice(Money.of(700, "BRL"))
                //.assetType()
                .dataProvider(providerID()).build();

        return Collections.singletonList(assetRepository.save(asset));
    }

    @Override
    public String retrieveCleanAssetTicker(String ticker) {
        return ticker;
    }
}
