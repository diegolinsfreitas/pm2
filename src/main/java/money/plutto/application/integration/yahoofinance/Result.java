package money.plutto.application.integration.yahoofinance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "symbol",
        "name",
        "exch",
        "type",
        "exchDisp",
        "typeDisp"
})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("name")
    private String name;
    @JsonProperty("exch")
    private String exch;
    @JsonProperty("type")
    private String type;
    @JsonProperty("exchDisp")
    private String exchDisp;
    @JsonProperty("typeDisp")
    private String typeDisp;

}