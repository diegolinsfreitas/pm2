package money.plutto.application.integration.yahoofinance;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "YahooAssetClient", url = "https://s.yimg.com/aq/autoc", fallback = YahooAssetClientFallback.class)
public interface YahooAssetClient {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    YahooResponse queryAsset(@RequestParam("query") String query, @RequestParam("region") String region, @RequestParam("lang") String lang);
}