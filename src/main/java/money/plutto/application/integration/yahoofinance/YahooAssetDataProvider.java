package money.plutto.application.integration.yahoofinance;

import money.plutto.application.integration.AssetDataProvider;
import money.plutto.application.model.Asset;
import money.plutto.application.repository.AssetRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.util.*;

@Service
@Slf4j
@AllArgsConstructor
public class YahooAssetDataProvider implements AssetDataProvider {

    private final AssetRepository assetRepository;


    private final YahooAssetClient yahooAssetClient;

    @Override
    public String providerID() {
        return "yahoo";
    }

    @Override
    @Transactional
    public void updateAssetsData(List<String> assetsCodes) throws IOException {
        if(assetsCodes.isEmpty()) {
            return;
        }
        Map<String, Stock> map = YahooFinance.get(assetsCodes.toArray(String[]::new));
        if(map.isEmpty()) {
            return;
        }
        List<Asset> assets = assetRepository.findAssetsBySymbolIsIn(map.values().stream().map(Stock::getSymbol).toArray(String[]::new));

        assets.forEach(result -> {
            Stock stock = map.get(result.getSymbol());
            result.setLastPrice(Money.of(stock.getQuote().getPrice(),stock.getCurrency()));
        });
    }

    @Override
    public Collection<? extends Asset> queryAsset(String criteria) {
        YahooResponse response = yahooAssetClient.queryAsset(criteria, "US", "en-US");
        if(response.getResultSet().getResult() == null || response.getResultSet().getResult().isEmpty()) {
            return Collections.emptyList();
        }
        List<Asset> assets = new ArrayList<>(response.getResultSet().getResult().size());

        response.getResultSet().getResult().stream().forEach(result -> {
            Asset saved = assetRepository.save(Asset.builder()
                    .name(result.getName())
                    .originalSymbol(retrieveCleanAssetTicker(result.getSymbol()))
                    .symbol(result.getSymbol()).build());
            assets.add(saved);
        });
        return assets;
    }

    @Override
    public String retrieveCleanAssetTicker(String ticker) {
        return ticker.split(".")[0];//ABEV3.SA become ABEV3
    }
}
