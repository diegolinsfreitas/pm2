package money.plutto.application.integration.yahoofinance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ResultSet"
})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YahooResponse {

    @JsonProperty("ResultSet")
    private ResultSet resultSet;
}
