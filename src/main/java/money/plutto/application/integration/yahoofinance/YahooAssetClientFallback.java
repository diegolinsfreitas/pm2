package money.plutto.application.integration.yahoofinance;


import java.util.Collections;

public class YahooAssetClientFallback implements YahooAssetClient {

    @Override
    public YahooResponse queryAsset(String query, String region, String lang) {
        return YahooResponse.builder().resultSet(ResultSet.builder().result(Collections.emptyList()).build()).build();
    }
}