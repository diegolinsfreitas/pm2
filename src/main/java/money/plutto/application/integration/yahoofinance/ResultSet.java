package money.plutto.application.integration.yahoofinance;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Query",
        "Result"
})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultSet {

    @JsonProperty("Query")
    private String query;
    @JsonProperty("Result")
    private List<Result> result = null;

}