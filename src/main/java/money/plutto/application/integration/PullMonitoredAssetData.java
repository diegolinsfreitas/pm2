package money.plutto.application.integration;

import money.plutto.application.model.Asset;
import money.plutto.application.repository.AssetRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@EnableAsync
@Service
@AllArgsConstructor
@Slf4j
public class PullMonitoredAssetData {

    private final List<AssetDataProvider> providers;
    
    private final CacheManager cacheManager;

    private final AssetRepository assetRepository;

    @Async
    @Scheduled(fixedRate = 900000)
    public void pullAssetData() {
        //cacheManager.getCache("MONITORED_ASSETS").put("ABEV3.SA", 1);
        List<String> assets = getMonitoredAssets();
        List<Asset> savedAssets = assetRepository.findAssetsBySymbolIsIn(assets.toArray(String[]::new));
            providers.forEach( p -> {
                List<String> originalSymbols = savedAssets.stream()
                        .filter(asset -> p.providerID().equals(asset.getDataProvider()))
                        .map(Asset::getOriginalSymbol).collect(Collectors.toList());
                try {
                    p.updateAssetsData(originalSymbols);
                } catch (IOException e) {
                    log.error("failed to update Assets", e);
                }
            });
    }

    public List<String> getMonitoredAssets() {
        Cache cache = cacheManager.getCache("MONITORED_ASSETS");
        if( cache == null) {
            return Collections.emptyList();
        }
        Object nativeCache = cache.getNativeCache();
        if (!(nativeCache instanceof javax.cache.Cache)) {
            return Collections.emptyList();
        }

        javax.cache.Cache<String, Integer> realCache = (javax.cache.Cache) nativeCache;
        return StreamSupport.stream(realCache.spliterator(), false)
                .filter( entry -> entry.getValue() > 0)
                .map(javax.cache.Cache.Entry::getKey)
                .collect(Collectors.toList());

    }
}
