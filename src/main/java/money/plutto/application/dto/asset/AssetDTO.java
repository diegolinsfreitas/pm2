package money.plutto.application.dto.asset;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import lombok.Data;

import javax.money.MonetaryAmount;

@Data
public class AssetDTO {

    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long id;

    private String symbol;

    private MonetaryAmount lastPrice;

    private String sector;

}
