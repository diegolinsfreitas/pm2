package money.plutto.application.dto.asset;

import money.plutto.application.dto.NonBuilderMapperConfig;
import money.plutto.application.dto.ReferenceMapper;
import money.plutto.application.model.AssetType;
import org.mapstruct.Mapper;


@Mapper(uses = {ReferenceMapper.class}, config = NonBuilderMapperConfig.class)
public interface AssetTypeMapper {

    AssetType toEntity(Long id);

    default Long toId(AssetType assetType) {
        return assetType.getId();
    }

}