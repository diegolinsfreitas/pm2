package money.plutto.application.dto.asset;

import money.plutto.application.dto.NonBuilderMapperConfig;
import money.plutto.application.dto.ReferenceMapper;
import money.plutto.application.model.Asset;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = {ReferenceMapper.class}, config = NonBuilderMapperConfig.class)
public interface AssetMapper {

    Asset toEntity(Long id);

    default Long toId(Asset asset) {
        return asset.getId();
    }

    List<AssetDTO> map(Iterable<Asset> assets);
}