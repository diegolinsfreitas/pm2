package money.plutto.application.dto;

import org.mapstruct.Builder;
import org.mapstruct.MapperConfig;

@MapperConfig(builder = @Builder(disableBuilder = true))
public interface NonBuilderMapperConfig {
}