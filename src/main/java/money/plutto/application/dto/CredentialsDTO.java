package money.plutto.application.dto;

import lombok.*;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CredentialsDTO {

    @NonNull
    @Email
    private String email;

    @NonNull
    private String password;

    private Boolean rememberMe;
}