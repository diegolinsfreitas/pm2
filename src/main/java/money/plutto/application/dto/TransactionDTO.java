package money.plutto.application.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import lombok.Builder;
import lombok.Data;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder
public class TransactionDTO {

    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long id;
    private BigDecimal quantity;
    private String type;
    private OffsetDateTime date;
    private MonetaryAmount totalTransaction;
    private MonetaryAmount sumTotal;
    private BigDecimal sumQuantity;
}
