package money.plutto.application.dto;

import lombok.Data;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
public class TransactionPutDTO {

    private BigDecimal quantity;
    private String type;
    private OffsetDateTime date;
    private MonetaryAmount totalTransaction;
}
