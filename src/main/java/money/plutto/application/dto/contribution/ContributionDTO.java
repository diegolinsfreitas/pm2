package money.plutto.application.dto.contribution;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import javax.money.MonetaryAmount;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.OffsetDateTime;

/**
 * Same dto used for post and put
 */
@Data
public class ContributionDTO {

    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long id;

    @NotNull
    private MonetaryAmount amount;

    @NotNull
    private LocalDate date;

}
