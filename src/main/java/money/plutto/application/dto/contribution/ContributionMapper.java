package money.plutto.application.dto.contribution;

import money.plutto.application.model.Contribution;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(nullValueMappingStrategy =  NullValueMappingStrategy.RETURN_DEFAULT)
public interface ContributionMapper {

    Contribution toEntity(ContributionDTO dto);

    List<ContributionDTO> map(Iterable<Contribution> contributions);

    ContributionDTO toDto(Contribution updated);
}