package money.plutto.application.dto;

import money.plutto.application.security.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;



@Mapper
@Component
public interface UserMapper {

    User toModel(UserPostDTO dto);

    //PortfolioDTO toDTO(Portfolio portfolio);

    //List<PortfolioDTO> map(List<Portfolio> portfolios);
}

