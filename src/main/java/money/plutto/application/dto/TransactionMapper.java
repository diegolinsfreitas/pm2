package money.plutto.application.dto;

import money.plutto.application.model.Transaction;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface TransactionMapper {

    Transaction toTransaction(TransactionPostDTO dto);

    Transaction toTransaction(TransactionPutDTO dto);

    TransactionDTO toDTO(Transaction transaction);

    List<TransactionDTO> map(List<Transaction> transactions);
}

