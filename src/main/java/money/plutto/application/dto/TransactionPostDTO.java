package money.plutto.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionPostDTO {

    private BigDecimal quantity;
    private String type;
    private OffsetDateTime date;
    private MonetaryAmount totalTransaction;

}
