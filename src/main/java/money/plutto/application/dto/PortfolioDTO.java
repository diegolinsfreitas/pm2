package money.plutto.application.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class PortfolioDTO {

    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long id;

    @NotBlank
    private String name;
}
