package money.plutto.application.dto.investment;

import money.plutto.application.dto.asset.AssetMapper;
import money.plutto.application.model.Investment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

import java.util.Set;


@Mapper(uses = {AssetMapper.class})
@Component
public interface InvestmentMapper {


    Investment toModel(InvestmentPutDTO dto);



    @Mappings({
            @Mapping(source = "investimentConsolidated.currentValue", target = "currentValue"),
            @Mapping(source = "investimentConsolidated.quantity", target = "quantity"),
            @Mapping(source = "investimentConsolidated.percentagePosition", target = "percentagePosition"),
    })
    InvestmentDTO toDTO(Investment dto);

    @Mapping(source = "assetId", target = "asset")
    Investment toModel(InvestmentPostDTO dto);

    Set<InvestmentDTO> map(Set<Investment> models);
}

