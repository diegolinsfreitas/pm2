package money.plutto.application.dto.investment;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InvestmentPutDTO {

    private Integer score;
    private String status;
}
