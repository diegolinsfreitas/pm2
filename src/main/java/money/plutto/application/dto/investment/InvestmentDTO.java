package money.plutto.application.dto.investment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import money.plutto.application.dto.asset.AssetDTO;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class InvestmentDTO {


    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long id;

    private AssetDTO asset;

    private String status;

    private Integer score;

    private BigDecimal quantity;

    private BigDecimal percentagePosition;

    private BigDecimal percentageTarget ;

    private BigDecimal percentageDiff;

    private MonetaryAmount monetaryDiff;

    private MonetaryAmount currentValue;

}
