package money.plutto.application.dto.investment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import money.plutto.application.security.idmask.IdMaskLongSerializers;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InvestmentPostDTO {

    @JsonSerialize(using = IdMaskLongSerializers.Serializer.class)
    @JsonDeserialize(using = IdMaskLongSerializers.Deserializer.class)
    private Long assetId;
}
