package money.plutto.application.dto;

import money.plutto.application.dto.asset.AssetTypeMapper;
import money.plutto.application.model.Portfolio;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper(uses = AssetTypeMapper.class)
@Component
public interface PortfolioMapper {

    @Mapping(source = "assetTypeId", target = "assetType")
    Portfolio toModel(PortfolioPostDTO dto);

    PortfolioDTO toDTO(Portfolio portfolio);

    List<PortfolioDTO> map(List<Portfolio> portfolios);
}

