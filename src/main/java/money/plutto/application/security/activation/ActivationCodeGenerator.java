package money.plutto.application.security.activation;

public interface ActivationCodeGenerator {

    String generate();

}
