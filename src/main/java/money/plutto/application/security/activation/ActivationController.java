package money.plutto.application.security.activation;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@AllArgsConstructor
public class ActivationController {

    private final ActivationService activationService;

    @GetMapping(value = "/activation/{username}")
    public ResponseEntity activateUserAndRedirectToLoginPage(@PathVariable String username, @RequestParam String id) {
        activationService.activate(username, id);
        return ResponseEntity.ok().build();//Maybe return to a page using view model
    }

}
