package money.plutto.application.security.activation;


import money.plutto.application.security.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivationCode {

    @Id
    @NotNull
    private String id;

    @OneToOne(optional = false)
    private User user;

    @Column(nullable = false)
    @CreationTimestamp
    private Date timestamp;

    private boolean sent = false;


}
