package money.plutto.application.security.activation;

import money.plutto.application.security.User;
import money.plutto.application.security.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@AllArgsConstructor
public class ActivationService {

    private final ActivationCodeRepository activationCodeRepository;

    private final UserRepository userRepository;

    public void activate(String username, String activationCodeId) {
        ActivationCode activationCode = findActivationCode(activationCodeId);
        validateActivationCode(username, activationCode);
        activateUser(activationCode);
        deleteActivationCode(activationCode);
    }

    private ActivationCode findActivationCode(String id) {
        return activationCodeRepository.findById(id)
            .orElseThrow(ActiviationException::new);
    }

    private void validateActivationCode(String username, ActivationCode activationCode) {
        if (isActivationRequestInvalid(activationCode, username)) {
            throw new ActiviationException();
        }
    }

    private boolean isActivationRequestInvalid(ActivationCode activationCode, String username) {
        return activationCode.getUser() == null ||
            !activationCode.getUser().getEmail().equalsIgnoreCase(username);
    }

    private void activateUser(ActivationCode activationCode) {
        User user = activationCode.getUser();
        user.setEnabled(true);
        userRepository.save(user);
    }

    private void deleteActivationCode(ActivationCode activationCode) {
        activationCodeRepository.delete(activationCode);
    }
}
