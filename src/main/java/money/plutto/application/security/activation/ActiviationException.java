package money.plutto.application.security.activation;

import money.plutto.application.exception.RestExceptionResponse;
import org.springframework.http.HttpStatus;

@RestExceptionResponse(
        status = HttpStatus.BAD_REQUEST,
        i18nMessage = "Não foi possível ativar sua conta."
)
public class ActiviationException extends RuntimeException{
}
