package money.plutto.application.security.idmask;

import at.favre.lib.idmask.IdMask;
import at.favre.lib.idmask.ext.IdMaskJackson;
import money.plutto.application.ApplicationContextProvider;

public final class IdMaskLongSerializers {

    public static final class Serializer extends IdMaskJackson.LongSerializer {
        public Serializer() {
            super(ApplicationContextProvider.getApplicationContext().getBean(IdMask.class));
        }
    }

    public static final class Deserializer extends IdMaskJackson.LongDeserializer {
        public Deserializer() {
            super(ApplicationContextProvider.getApplicationContext().getBean(IdMask.class));
        }
    }
}