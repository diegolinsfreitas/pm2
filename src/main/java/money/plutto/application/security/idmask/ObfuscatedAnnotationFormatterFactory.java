package money.plutto.application.security.idmask;

import at.favre.lib.idmask.IdMask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.EmbeddedValueResolutionSupport;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Formatter;
import org.springframework.format.Parser;
import org.springframework.format.Printer;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;
import java.util.Set;


@Component
public class ObfuscatedAnnotationFormatterFactory extends EmbeddedValueResolutionSupport
        implements AnnotationFormatterFactory<Obfuscated> {

    @Autowired
    private IdMask<Long> idMask;

    @Override
    public Set<Class<?>> getFieldTypes() {
        return Set.of(Long.class);
    }

    @Override
    public Printer<Long> getPrinter(Obfuscated annotation, Class<?> fieldType) {
        return configureFormatterFrom(annotation);
    }

    @Override
    public Parser<Long> getParser(Obfuscated annotation, Class<?> fieldType) {
        return configureFormatterFrom(annotation);
    }

    private Formatter<Long> configureFormatterFrom(Obfuscated annotation) {

        return new Formatter<>() {

            @Override
            public String print(Long aLong, Locale locale) {
                return idMask.mask(aLong);
            }

            @Override
            public Long parse(String text, Locale locale) throws ParseException {
                return idMask.unmask(text);
            }
        };
    }
}