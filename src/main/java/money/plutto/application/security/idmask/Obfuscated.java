package money.plutto.application.security.idmask;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * All resources id are obfuscated (local profile is disabled) and this annotation is used
 * to tell spring to uncrypt the data tha comes in via path variables
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Obfuscated {
}
