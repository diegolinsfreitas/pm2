package money.plutto.application.security.email;


public interface SenderService {

    void sendEmail(EmailMessage emailMessage) throws Exception;

}
