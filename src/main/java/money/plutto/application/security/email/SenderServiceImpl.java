package money.plutto.application.security.email;

import money.plutto.application.security.activation.ActiviationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
@Slf4j
public class SenderServiceImpl implements SenderService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Value("${plutto.mail.activation.title}")
    private String subject;

    @Override
    @Async
    public void sendEmail(EmailMessage emailMessage) throws MessagingException {
        try {
            tryParseAndSendEmail(emailMessage);
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void tryParseAndSendEmail(EmailMessage emailMessage) throws MessagingException {
        MimeMessage mail = javaMailSender.createMimeMessage();
        parseMessage(emailMessage, mail);
        javaMailSender.send(mail);
    }

    private void parseMessage(EmailMessage emailMessage,
        MimeMessage mail) throws MessagingException {
        MimeMessageHelper messageHelper = new MimeMessageHelper(mail, true);
        messageHelper.setFrom(fromEmail);
        messageHelper.setTo(emailMessage.getRecipient());
        messageHelper.setSubject(emailMessage.getSubject());
        messageHelper.setText(emailMessage.getContent(), true);
    }

    private void handleException(Exception e) {
        log.error("Mail Send Exception - smtp service unavailable");
        e.printStackTrace();
        throw new ActiviationException();
    }
}
