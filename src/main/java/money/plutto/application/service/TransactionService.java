package money.plutto.application.service;

import money.plutto.application.exception.InvestmentNotFoundException;
import money.plutto.application.model.Investment;
import money.plutto.application.model.Transaction;
import money.plutto.application.repository.InvestmentRepository;
import money.plutto.application.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;

    private final InvestmentRepository investmentRepository;


    public List<Transaction> getTransactions(Long investmentId) {
        List<Transaction> allByInvestment_id = transactionRepository.findAllByInvestment_Id(investmentId);
        allByInvestment_id.stream().reduce(null, Transaction::aggregateTotalValue);
        allByInvestment_id.stream().reduce(null, Transaction::aggregateQuantity);
        return allByInvestment_id;
    }

    public void consolidateTransactionData(Investment investment, List<Transaction> transactions) {
        consolidateTransactionData(transactions);
        investment.getInvestimentConsolidated().setQuantity(transactions.get(transactions.size()-1).getSumQuantity());
    }

    public void consolidateTransactionData(List<Transaction> transactions) {
        transactions.stream().reduce(null, Transaction::aggregateTotalValue);
        transactions.stream().reduce(null, Transaction::aggregateQuantity);
    }

    public void deleteTransaction(Long investmentId) {
        transactionRepository.delete(transactionRepository.getOne(investmentId));
    }

    public void saveTransaction(Transaction toTransaction, Long investmentId) {
        addTransaction(investmentId, toTransaction);
        transactionRepository.save(toTransaction);
    }

    public void addTransaction(Long investmentId, Transaction newTransaction) {
        Optional<Investment> investment = investmentRepository.findById(investmentId);
        if (investment.isEmpty()) {
            throw new InvestmentNotFoundException();
        }
        newTransaction.setInvestment(investment.get());
        transactionRepository.save(newTransaction);
    }

    public Transaction updateTransaction(Long transactionId, Transaction toTransaction) {
        Transaction transaction = transactionRepository.getOne(transactionId);
        transaction.setQuantity(toTransaction.getQuantity());
        transaction.setType(toTransaction.getType());
        transaction.setTotalTransaction(toTransaction.getTotalTransaction());
        return transaction;
    }
}
