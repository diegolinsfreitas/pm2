package money.plutto.application.service;

import money.plutto.application.security.User;
import money.plutto.application.security.UserRepository;
import money.plutto.application.security.activation.ActivationCode;
import money.plutto.application.security.activation.ActivationCodeGenerator;
import money.plutto.application.security.activation.ActivationCodeRepository;
import money.plutto.application.security.email.ActivationMessageGenerator;
import money.plutto.application.security.email.EmailMessage;
import money.plutto.application.security.email.SenderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@AllArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final ActivationCodeRepository repository;

    private final SenderService senderService;

    private final ActivationCodeGenerator activationCodeGenerator;

    private final ActivationMessageGenerator activationMessageGenerator;

    public User save(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public void create(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        String generatedCode = activationCodeGenerator.generate();
        ActivationCode activationCode = ActivationCode.builder().id(generatedCode).build();
        user.setEnabled(false);
        activationCode.setUser(user);
        EmailMessage emailMessage = activationMessageGenerator.generate(activationCode);
        try {
            senderService.sendEmail(emailMessage);
            activationCode.setSent(true);
            userRepository.save(user);
            repository.save(activationCode);
        } catch (Exception e) {
            log.error("error sending email", e);
        }
    }

}
