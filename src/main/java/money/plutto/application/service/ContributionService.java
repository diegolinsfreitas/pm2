package money.plutto.application.service;

import lombok.AllArgsConstructor;
import money.plutto.application.exception.InvestmentNotFoundException;
import money.plutto.application.model.Contribution;
import money.plutto.application.model.Investment;
import money.plutto.application.model.Transaction;
import money.plutto.application.repository.ContributionsRepository;
import money.plutto.application.repository.InvestmentRepository;
import money.plutto.application.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ContributionService {

    private final ContributionsRepository contributionsRepository;

    private final InvestmentRepository investmentRepository;

    public List<Contribution> findAll() {
        return contributionsRepository.findAll();
    }

    public void delete(Long id) {
        contributionsRepository.delete(contributionsRepository.getOne(id));
    }

    public void save(Contribution model) {
        contributionsRepository.save(model);
    }

    public Contribution update(Long id, Contribution model) {
        Contribution reference = contributionsRepository.getOne(id);
        reference.setAmount(model.getAmount());
        reference.setDate(model.getDate());
        return reference;
    }
}
