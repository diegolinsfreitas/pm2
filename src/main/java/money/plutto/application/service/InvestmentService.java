package money.plutto.application.service;

import money.plutto.application.exception.InvestmentNotFoundException;
import money.plutto.application.exception.PortfolioNotFoundException;
import money.plutto.application.model.Investment;
import money.plutto.application.model.Portfolio;
import money.plutto.application.repository.InvestmentRepository;
import money.plutto.application.repository.PortfolioRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class InvestmentService {

    private final PortfolioRepository portfolioRepository;

    private final InvestmentRepository investmentRepository;

    public Set<Investment> getAssetsByPortfolio(Long portfolioID) {
        Optional<Portfolio> optional = portfolioRepository.findById(portfolioID);//assets are eager loaded
        if(optional.isEmpty()) {
            throw new PortfolioNotFoundException();
        }
        Set<Investment> assets = optional.get().getInvestments();
        optional.get().calculateCurrentValueAndPositions();
        Set<Investment> result = new HashSet<>(assets);
        return  result;
    }

    public Investment updateInvestment(Long investmentId, Investment toModel) {
        Optional<Investment> investment = investmentRepository.findById(investmentId);
        if(investment.isEmpty()){
            throw new InvestmentNotFoundException();
        }
        Investment inv = investment.get();
        inv.setScore(toModel.getScore());
        inv.setStatus(toModel.getStatus());
        investmentRepository.save(inv);
        return inv;
    }

    public Investment saveInvestment(Investment toModel, Long portfolioId) {
        Portfolio one = portfolioRepository.getOne(portfolioId);
        if (one == null) {
            throw new PortfolioNotFoundException();
        }
        toModel.setPortfolio(one);
        return investmentRepository.save(toModel);
    }

    public void delete(Long investmentId) {
        Optional<Investment> investment = investmentRepository.findById(investmentId);
        if(investment.isEmpty()) {
            throw new InvestmentNotFoundException();
        }
        investmentRepository.delete(investment.get());
    }
}
