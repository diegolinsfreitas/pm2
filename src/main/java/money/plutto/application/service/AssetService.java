package money.plutto.application.service;

import money.plutto.application.integration.AssetDataProvider;
import money.plutto.application.model.Asset;
import money.plutto.application.repository.AssetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AssetService {

    private final AssetRepository assetRepository;

    private final List<AssetDataProvider> assetDataProviders;


    public List<Asset> findAssetByName(String criteria) {
        List<Asset> assets = assetRepository.findAssetsBySymbolContains(criteria);
        if(assets.isEmpty()) {
            return assetDataProviders
                    .parallelStream()
                    .map( ap-> ap.queryAsset(criteria))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

        }
        return assets;
    }

    public Iterable<Asset> getAssets() {
        return assetRepository.findAll();
    }
}
