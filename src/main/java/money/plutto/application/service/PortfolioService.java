package money.plutto.application.service;



import money.plutto.application.exception.PortfolioNotFoundException;
import money.plutto.application.model.Asset;
import money.plutto.application.model.Investment;
import money.plutto.application.model.Portfolio;
import money.plutto.application.repository.AssetRepository;
import money.plutto.application.repository.InvestmentRepository;
import money.plutto.application.repository.PortfolioRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Slf4j
@AllArgsConstructor
public class PortfolioService {

    private final PortfolioRepository portfolioRepository;

    private final AssetRepository assetRepository;

    private final InvestmentRepository investmentRepository;


    public Investment addInvestmentToPortfolio(Long portfolioId, Long assetId) {
        Optional<Portfolio> portfolioOptional = portfolioRepository.findById(portfolioId);
        if(portfolioOptional.isEmpty()) {
            throw new PortfolioNotFoundException();
        }

        Optional<Asset> assetById = assetRepository.findById(assetId);
        if(assetById.isEmpty()) {
            throw new RuntimeException("ticker-do-not-exist");
        }

        Investment addAsset = portfolioOptional.get().addInvestment(assetById.get());
        return investmentRepository.save(addAsset);
    }

    public List<Portfolio> getPortfolios() {
        return StreamSupport
                .stream(portfolioRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Portfolio getPortfolioById(Long portfolioId) {
        Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);
        if (portfolio.isEmpty()) {
           throw new PortfolioNotFoundException();
        }

        return portfolio.get();
    }

    public Long savePortfolio(Portfolio toModel) {
        Portfolio saved = portfolioRepository.save(toModel);
        return saved.getId();
    }

    public void deletePortfolio(Long portfolioId) {
        portfolioRepository.deleteById(portfolioId);
    }
}
