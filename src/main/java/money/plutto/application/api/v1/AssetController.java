package money.plutto.application.api.v1;

import money.plutto.application.dto.asset.AssetDTO;
import money.plutto.application.dto.asset.AssetMapper;
import money.plutto.application.service.AssetService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(AssetController.BASE_URI)
@RestController
@Validated
@AllArgsConstructor
public class AssetController {

    public static final String BASE_URI = "/assets/";

    private final AssetService assetService;

    private final AssetMapper assetMapper;

    @GetMapping
    public ResponseEntity<List<AssetDTO>> getAssets() {
        return ResponseEntity.ok(assetMapper.map(assetService.getAssets()));
    }

}