package money.plutto.application.api.v1;

import money.plutto.application.dto.investment.InvestmentDTO;
import money.plutto.application.dto.investment.InvestmentPostDTO;
import money.plutto.application.dto.investment.InvestmentPutDTO;
import money.plutto.application.dto.investment.InvestmentMapper;
import money.plutto.application.model.Investment;
import money.plutto.application.security.idmask.Obfuscated;
import money.plutto.application.service.InvestmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

@RequestMapping(InvestmentController.BASE_URI)
@RestController
@Validated
@AllArgsConstructor
public class InvestmentController {

    public static final String BASE_URI = "/portfolios/{portfolioId}/investments/";

    private final InvestmentService investmentService;

    private final InvestmentMapper investmentMapper;

    @GetMapping
    public ResponseEntity<Set<InvestmentDTO>> getInvestmentsByPortfolioId(@NotNull @PathVariable("portfolioId") @Obfuscated Long portfolioId) {
        return ResponseEntity.ok(investmentMapper.map(investmentService.getAssetsByPortfolio(portfolioId)));
    }

    @PutMapping(path = "{investmentId}")
    public ResponseEntity handlePut(@NotNull @PathVariable("investmentId") @Obfuscated Long investmentId, @Valid @RequestBody InvestmentPutDTO dto) {
        Investment updated = investmentService.updateInvestment(investmentId, investmentMapper.toModel(dto));
        return ResponseEntity.ok(investmentMapper.toDTO(updated));
    }

    @PostMapping
    public ResponseEntity handlePost(@NotNull @PathVariable("portfolioId") @Obfuscated Long portfolioId, @Valid @RequestBody InvestmentPostDTO dto) {
        investmentService.saveInvestment(investmentMapper.toModel(dto), portfolioId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping(path = "{investmentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@NotNull @PathVariable("investmentId") @Obfuscated Long investmentId){
        investmentService.delete(investmentId);
    }
}