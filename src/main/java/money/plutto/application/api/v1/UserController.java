package money.plutto.application.api.v1;

import money.plutto.application.dto.UserMapper;
import money.plutto.application.dto.UserPostDTO;
import money.plutto.application.security.User;
import money.plutto.application.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserDetailsService userDetailsService;

    private final UserService userService;

    private final UserMapper userMapper;

    @GetMapping
    public User getUser(Authentication authentication) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
        Map<String, Object> attributes = token.getTokenAttributes();
        //TODO add mapper for user
        //TODO hide password hash
        return (User) userDetailsService.loadUserByUsername(attributes.get("username").toString());
    }

    @PostMapping("/register")
    public ResponseEntity create(@Valid @RequestBody UserPostDTO newUser) {

        log.info("CREATE NEW USER: {}", newUser.getEmail());
        userService.create(userMapper.toModel(newUser));
        return new ResponseEntity(HttpStatus.CREATED);
    }

}