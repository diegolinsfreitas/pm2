package money.plutto.application.api.v1;

import money.plutto.application.dto.*;
import money.plutto.application.model.Transaction;
import money.plutto.application.security.idmask.Obfuscated;
import money.plutto.application.service.TransactionService;
import lombok.AllArgsConstructor;
import money.plutto.application.dto.TransactionDTO;
import money.plutto.application.dto.TransactionMapper;
import money.plutto.application.dto.TransactionPostDTO;
import money.plutto.application.dto.TransactionPutDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequestMapping(TransactionController.BASE_URI)
@RestController
@Validated
@AllArgsConstructor
public class TransactionController {

    public static final String BASE_URI = "/investments/{investmentId}/transactions/";

    private final TransactionService transactionService;

    private final TransactionMapper transactionMapper;

    @GetMapping
    public ResponseEntity<List<TransactionDTO>> getInvestmentsByPortfolioId(@NotNull @PathVariable("investmentId") @Obfuscated Long investmentId ) {
        return ResponseEntity.ok(transactionMapper.map(transactionService.getTransactions(investmentId)));
    }

    @PutMapping(path = "{transactionId}")
    public ResponseEntity<TransactionDTO> handlePut(@NotNull @PathVariable("investmentId") @Obfuscated Long investmentId,
                                                    @NotNull @PathVariable("transactionId") @Obfuscated Long transactionId,
                                                    @Valid @RequestBody TransactionPutDTO dto) {
        Transaction updated = transactionService.updateTransaction(transactionId, transactionMapper.toTransaction(dto));
        return ResponseEntity.ok(transactionMapper.toDTO(updated));
    }

    @PostMapping
    public ResponseEntity<TransactionDTO> handlePost(@NotNull @PathVariable("investmentId") @Obfuscated Long investmentId, @Valid @RequestBody TransactionPostDTO dto) {
        transactionService.saveTransaction(transactionMapper.toTransaction(dto), investmentId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping(path = "{transactionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@NotNull @PathVariable("transactionId") @Obfuscated Long transactionId){
        transactionService.deleteTransaction(transactionId);
    }
}