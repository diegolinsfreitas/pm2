package money.plutto.application.api.v1;

import money.plutto.application.dto.PortfolioDTO;
import money.plutto.application.dto.PortfolioMapper;
import money.plutto.application.dto.PortfolioPostDTO;
import money.plutto.application.logging.ApplicationFlowTrackingID;
import money.plutto.application.security.idmask.Obfuscated;
import money.plutto.application.service.PortfolioService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RequestMapping(PortfolioController.BASE_URI)
@RestController
@Validated
@Slf4j
@ApplicationFlowTrackingID
@AllArgsConstructor
public class PortfolioController {

    public static final String BASE_URI = "/portfolios/";

    private final PortfolioService portfolioService;

    private final PortfolioMapper portfolioMapper;

    @GetMapping(path = "{portfolioId}")
    public ResponseEntity<PortfolioDTO> getPortifolioById(
            @NotNull
            @PathVariable("portfolioId")
            @Obfuscated Long portfolioId) {
        return ResponseEntity.ok(portfolioMapper.toDTO(portfolioService.getPortfolioById(portfolioId)));
    }

    @GetMapping
    public ResponseEntity<List<PortfolioDTO>> getPortifolios() {
        log.info("Get Portfolios");
        return ResponseEntity
                .ok()
                //.cacheControl(CacheControl.maxAge(15, TimeUnit.MINUTES))
                .body(portfolioMapper.map(portfolioService.getPortfolios()));
    }

    @PostMapping
    public ResponseEntity handlePost(@Valid @RequestBody PortfolioPostDTO dto) {
        Long portfolioId = portfolioService.savePortfolio(portfolioMapper.toModel(dto));
        return ResponseEntity.created(URI.create(BASE_URI+portfolioId)).build();
    }


    @DeleteMapping(path = "{portfolioId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@NotNull @PathVariable("portfolioId") @Obfuscated Long portfolioId){
        portfolioService.deletePortfolio(portfolioId);
    }

}