package money.plutto.application.api.v1;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import money.plutto.application.dto.CredentialsDTO;
import money.plutto.application.dto.LoginResult;
import money.plutto.application.security.JwtHelper;
import money.plutto.application.security.User;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * The auth controller to handle login requests
 *
 * @author diego
 */
@RestController
@AllArgsConstructor
public class AuthController {

    private final JwtHelper jwtHelper;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtDecoder jwtDecoder;

    @PostMapping(path = "login", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public LoginResult login(@Valid @RequestBody CredentialsDTO credentials) {

        UserDetails userDetails;
        try {
            userDetails = userDetailsService.loadUserByUsername(credentials.getEmail());
        } catch (UsernameNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not found");
        }

        if (passwordEncoder.matches(credentials.getPassword(), userDetails.getPassword())) {
            Map<String, String> claims = new HashMap<>();
            claims.put("email", credentials.getEmail());
            claims.put("name", ((User)userDetails).getName());

            String authorities = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.joining(","));
            claims.put("authorities", authorities);
            claims.put("id", String.valueOf(1));

            String jwt = jwtHelper.createJwtForClaims(credentials.getEmail(), claims);
            return new LoginResult(jwt);
        }

        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not authenticated");
    }

    @PostMapping("/refreshtoken")
    public LoginResult refreshtoken(@RequestBody String token) {

        Jwt decode = jwtDecoder.decode(token);
        String jwt = jwtHelper.createJwtForClaims(decode.getSubject(), decode.getClaims().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toString())));
        return new LoginResult(jwt);
    }
}