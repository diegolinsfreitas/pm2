package money.plutto.application.api.v1;

import lombok.AllArgsConstructor;
import money.plutto.application.dto.contribution.ContributionDTO;
import money.plutto.application.dto.contribution.ContributionMapper;
import money.plutto.application.model.Contribution;

import money.plutto.application.security.idmask.Obfuscated;
import money.plutto.application.service.ContributionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequestMapping(ContributionsController.BASE_URI)
@RestController
@Validated
@AllArgsConstructor
public class ContributionsController {

    public static final String BASE_URI = "/contributions/";

    private final ContributionService contributionService;

    private final ContributionMapper contributionMapper;

    @GetMapping
    public ResponseEntity<List<ContributionDTO>> getAll() {
        return ResponseEntity.ok(contributionMapper.map(contributionService.findAll()));
    }

    @PutMapping(path = "{contributionId}")
    public ResponseEntity<ContributionDTO> handlePut(@NotNull @PathVariable("contributionId") @Obfuscated Long contributionId,
                                                    @Valid @RequestBody ContributionDTO dto) {
        Contribution updated = contributionService.update(contributionId, contributionMapper.toEntity(dto));
        return ResponseEntity.ok(contributionMapper.toDto(updated));
    }

    @PostMapping
    public ResponseEntity<ContributionDTO> handlePost(@Valid @RequestBody ContributionDTO dto) {
        contributionService.save(contributionMapper.toEntity(dto));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping(path = "{contributionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@NotNull @PathVariable("contributionId") @Obfuscated Long contributionId){
        contributionService.delete(contributionId);
    }
}