package money.plutto.application.exception;

import org.springframework.http.HttpStatus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RestExceptionResponse {
    HttpStatus status() default HttpStatus.INTERNAL_SERVER_ERROR;
    String i18nMessage() default "Estamos com problemas técnicos";
}
