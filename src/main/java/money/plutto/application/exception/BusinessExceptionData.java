package money.plutto.application.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;

@Data
@Builder
@AllArgsConstructor
class BusinessExceptionData {

    private HttpStatus status;
    private OffsetDateTime timestamp;
    private String message;

}