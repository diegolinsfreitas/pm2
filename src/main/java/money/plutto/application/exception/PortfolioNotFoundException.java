package money.plutto.application.exception;

import org.springframework.http.HttpStatus;

@RestExceptionResponse(
        status = HttpStatus.NOT_FOUND,
        i18nMessage = "Portifólio não existe"
)
public class PortfolioNotFoundException extends RuntimeException{ }
