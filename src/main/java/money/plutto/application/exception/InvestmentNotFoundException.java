package money.plutto.application.exception;

import org.springframework.http.HttpStatus;

@RestExceptionResponse(
        status = HttpStatus.NOT_FOUND,
        i18nMessage = "Investimento não existe no seu portifólio"
)
public class InvestmentNotFoundException extends RuntimeException {
}
