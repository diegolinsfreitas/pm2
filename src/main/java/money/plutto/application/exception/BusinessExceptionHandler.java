package money.plutto.application.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class BusinessExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Throwable.class)
    protected ResponseEntity<Object> handleBusinessException(
            Throwable ex) {
        if(ex.getClass().isAnnotationPresent(RestExceptionResponse.class)){
            RestExceptionResponse[] annotationsByType = ex.getClass().getAnnotationsByType(RestExceptionResponse.class);
            BusinessExceptionData exception = BusinessExceptionData.builder()
                    .status(annotationsByType[0].status())
                    .timestamp(OffsetDateTime.now())
                    .message(annotationsByType[0].i18nMessage())
                    .build();
            return buildResponseEntity(exception);
        }
        log.error("could not handle this exception", ex);
        return buildResponseEntity(BusinessExceptionData.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .timestamp(OffsetDateTime.now())
                .message(ex.getMessage())
                .build());
    }

    private ResponseEntity<Object> buildResponseEntity(BusinessExceptionData error) {
        return new ResponseEntity<>(error, error.getStatus());
    }
}
