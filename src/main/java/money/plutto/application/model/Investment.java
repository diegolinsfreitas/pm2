package money.plutto.application.model;


import money.plutto.application.MonetaryAmountConverter;
import money.plutto.application.security.User;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;
import org.javamoney.moneta.Money;
import org.jetbrains.annotations.NotNull;

import javax.money.MonetaryAmount;
import javax.persistence.*;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@DynamicUpdate
@EntityListeners(UserAwareListener.class)
public class Investment implements UserAware{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @EqualsAndHashCode.Include
    @ManyToOne
    private Asset asset;

    @OneToMany(mappedBy = "investment", fetch = FetchType.EAGER)
    private List<Transaction> transactions;

    @ManyToOne
    private User user;

    /**
     * used to calculate the percentageTarget
     *
     * it is easier for the user to define investment weights in its own scale than use
     * small percentage changes like 2.34%, 5.5%, because it is not limited by the 100% range
     *
     * he is free to add assets and set the score without worrying about to match the exact 100% allocation
     * eg:
     *
     * APPL score 80;
     * GOOG score 60;
     *
     * Sum of scores is 140
     *
     * APPL target percentage is: 57%
     * GOOG target percentage is: 43%
     */
    private BigDecimal score;

    //TODO must be calculated based on the score
    @Transient
    private BigDecimal percentageTarget = BigDecimal.ZERO;

    @Transient
    private BigDecimal percentageDiff = BigDecimal.ZERO;

    @Transient
    private MonetaryAmount monetaryDiff = MonetaryAmountConverter.ZERO;

    private InvestmentStatus status;

    @ManyToOne(fetch=FetchType.LAZY)
    private Portfolio portfolio;

    @Embedded
    private InvestimentConsolidated investimentConsolidated = new InvestimentConsolidated();

    @Version
    private Integer version;

    @CreationTimestamp
    private OffsetDateTime createdDate;

    @UpdateTimestamp
    private OffsetDateTime lastModifiedDate;


    private MonetaryAmount calculateLastMarketValue() {
        return asset.getLastPrice().multiply(this.investimentConsolidated.getQuantity());
    }

    public void calculatePositions() {
        if (transactions.isEmpty()) {
            this.investimentConsolidated.setCurrentValue(Money.of(0, "BRL"));
            return;
        }

        transactions.stream().reduce(null, Transaction::aggregateQuantity);
        this.investimentConsolidated.setQuantity(transactions.get(transactions.size()-1).getSumQuantity());
        //TODO for fixed income the user will update this field

        this.investimentConsolidated.setCurrentValue(calculateLastMarketValue());

    }

    public void calculateMetrics(BigDecimal sumScores) {
        percentageDiff = BigDecimal.ZERO;
        percentageTarget = score.divide(sumScores);

        if(!BigDecimal.ZERO.equals(this.investimentConsolidated.getQuantity())) {
            this.investimentConsolidated.setMeanPrice(this.getInvestimentConsolidated().getCurrentValue().divide(this.investimentConsolidated.getQuantity()));
        }
        this.investimentConsolidated.setPercentagePosition(BigDecimal.valueOf(investimentConsolidated.getCurrentValue().divide(this.portfolio.getCurrentValue().getNumber()).getNumber().doubleValue()));
        this.percentageDiff = this.investimentConsolidated.getPercentagePosition().subtract(this.percentageTarget);
        if(!BigDecimal.ZERO.setScale(2).equals(percentageTarget) ) {
            this.monetaryDiff = calculateSuggestedPurchaseValue();
        }
    }

    @NotNull
    private MonetaryAmount calculateSuggestedPurchaseValue() {
        //TODO save currency using value of transaction from the first transaction.
        //review this calc
        return Money.of(this.percentageDiff, "BRL").multiply(this.portfolio.getCurrentValue().getNumber());
    }


}