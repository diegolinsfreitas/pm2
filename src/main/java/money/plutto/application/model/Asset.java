package money.plutto.application.model;

import money.plutto.application.PersistentMoneyAmountAndCurrency;
import lombok.*;
import org.hibernate.annotations.*;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.persistence.Entity;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@TypeDef(name = "money", typeClass = PersistentMoneyAmountAndCurrency.class)
@DynamicUpdate
public class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Include
    @Column(unique = true)
    private String symbol;

    private String originalSymbol;

    private String name;

    private String dataProvider;

    @ManyToOne
    private AssetType assetType;

    @Columns(columns = {@Column(name = "last_price_currency", length = 3), @Column(name = "last_price_value", precision = 4, scale = 2)})
    @Type(type = "money")
    private MonetaryAmount lastPrice;

    private String sector;

    @Version
    @Column(columnDefinition = "integer default 0")
    private Integer version;

    @CreationTimestamp
    private OffsetDateTime createdDate;

    @UpdateTimestamp
    private OffsetDateTime lastModifiedDate;

}
