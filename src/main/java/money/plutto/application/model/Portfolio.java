package money.plutto.application.model;


import money.plutto.application.PersistentMoneyAmountAndCurrency;
import money.plutto.application.security.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "money", typeClass = PersistentMoneyAmountAndCurrency.class)
@EntityListeners(UserAwareListener.class)
public class Portfolio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    private Boolean consolidated;

    @ManyToOne
    @NotNull
    private AssetType assetType;

    @Columns(columns = {@Column(name = "current_value_currency", length = 3), @Column(name = "current_value_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount currentValue;

    @OneToMany(mappedBy="portfolio", fetch = FetchType.EAGER)
    private Set<Investment> investments;

    public void calculateCurrentValueAndPositions() {
        if(investments.isEmpty()) {
            return;
        }
        this.currentValue = Monetary.getDefaultAmountFactory().setNumber(BigDecimal.ZERO).setCurrency("BRL").create();

        Stream<Investment> assetStream = this.investments.stream();

        assetStream.forEach(a -> {
            a.calculatePositions();
            this.currentValue = this.currentValue.add(a.getInvestimentConsolidated().getCurrentValue());
        });
        List<Investment> investmentsWithScores = this.investments.stream()
                .filter(investment -> investment.getScore() != null).collect(Collectors.toList());
        BigDecimal sumScores = investmentsWithScores.stream()
                .filter(investment -> investment.getScore() != null )
                .map(Investment::getScore)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        for(Investment i: investmentsWithScores) {
            i.calculateMetrics(sumScores);
        }
    }

    @OneToMany(mappedBy = "portfolio")
    private List<Contribution> contributions;

    @ManyToOne
    private User user;

    @Version
    private Integer version;

    @CreationTimestamp
    private OffsetDateTime createdDate;

    @UpdateTimestamp
    private OffsetDateTime lastModifiedDate;

    public Investment addInvestment(Asset asset) {
        Investment investment = Investment.builder()
                .asset(asset)
                .portfolio(this).build();
        if(!investments.add(investment)) {
            throw new RuntimeException("asset-already-in-portfolio");
        }
        return investment;
    }
}