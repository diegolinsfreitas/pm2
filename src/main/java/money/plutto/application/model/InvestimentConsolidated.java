package money.plutto.application.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Type;

import javax.money.MonetaryAmount;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@Data
@NoArgsConstructor
public class InvestimentConsolidated {

    @Formula("0")
    private BigDecimal quantity;

    @Columns(columns = {@Column(name = "mean_price_currency", length = 3), @Column(name = "mean_price_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount meanPrice;

    private BigDecimal percentagePosition;

    private BigDecimal averageSellPrice;

    private BigDecimal totalCost;

    private BigDecimal totalDividend;

    @Columns(columns = {@Column(name = "current_value_currency", length = 3), @Column(name = "current_value_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount currentValue;

}
