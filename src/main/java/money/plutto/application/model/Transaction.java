package money.plutto.application.model;

import money.plutto.application.PersistentMoneyAmountAndCurrency;
import money.plutto.application.security.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "money", typeClass = PersistentMoneyAmountAndCurrency.class)
@EntityListeners(UserAwareListener.class)
public class Transaction implements UserAware {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private TransactionType type;

    @OrderBy
    private OffsetDateTime date;

    private BigDecimal quantity;

    @Columns(columns = {@Column(name = "amount_currency", length = 3), @Column(name = "amount_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount totalTransaction;

    @Columns(columns = {@Column(name = "unit_cost_currency", length = 3), @Column(name = "unit_cost_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount unitCost;

    @ManyToOne
    private Investment investment;

    @Transient
    private BigDecimal sumQuantity;

    @Transient
    private MonetaryAmount sumTotal;

    @ManyToOne
    private User user;

    @Version
    private Integer version;

    @CreationTimestamp
    private OffsetDateTime createdDate;

    @UpdateTimestamp
    private OffsetDateTime lastModifiedDate;

    public static Transaction aggregateTotalValue(Transaction transaction, Transaction transaction1) {
        if(transaction == null ){
            transaction1.setSumTotal(transaction1.getTotalTransaction());
        }else {
            transaction1.setSumTotal(transaction.getTotalTransaction().add(transaction1.getTotalTransaction()));
        }
        return transaction1;
    }

    public static Transaction aggregateQuantity(Transaction transaction, Transaction transaction1) {
        if(transaction == null ){
            transaction1.setSumQuantity(transaction1.getQuantity());
        }else {
            transaction1.setSumQuantity(transaction1.type.getSumImpl().calculateCurrentSumOfQuantity(transaction, transaction1));
        }
        return transaction1;
    }

}