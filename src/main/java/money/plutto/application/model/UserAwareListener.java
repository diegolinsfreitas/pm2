package money.plutto.application.model;

import money.plutto.application.ApplicationContextProvider;
import money.plutto.application.security.User;
import money.plutto.application.security.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.PrePersist;

public class UserAwareListener {

    @PrePersist
    public void methodExecuteBeforeSave(Object reference) {
        if(reference instanceof UserAware) {
            UserRepository bean = ApplicationContextProvider.getApplicationContext().getBean(UserRepository.class);
            User user = bean.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
            ((UserAware) reference).setUser(user);
        }
    }
}
