package money.plutto.application.model;


import money.plutto.application.PersistentMoneyAmountAndCurrency;
import money.plutto.application.security.User;
import lombok.*;
import org.hibernate.annotations.*;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@TypeDef(name = "money", typeClass = PersistentMoneyAmountAndCurrency.class)
@EntityListeners(UserAwareListener.class)
public class Contribution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Columns(columns = {@Column(name = "contribution_currency", length = 3), @Column(name = "contribution_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount amount;

    private LocalDate date;

    @ManyToOne
    private Portfolio portfolio;

    @ManyToOne
    private User user;

    @Version
    private Integer version;

    @CreationTimestamp
    private OffsetDateTime createdDate;

    @UpdateTimestamp
    private OffsetDateTime lastModifiedDate;
}
