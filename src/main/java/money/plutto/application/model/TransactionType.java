package money.plutto.application.model;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public enum TransactionType {
    BUY(new DefaultSumQuantity()),
    SELL(new DefaultSumQuantity()),//TODO implement sell summary quantity and total value
    SPLIT(new DefaultSumQuantity()),//TODO implement sell summary quantity
    GROUP(new DefaultSumQuantity()),//TODO implement sell summary quantity
    BONUS(new DefaultSumQuantity());//TODO implement sell summary quantity

    private final CalculateSumOfQuantity sumImpl;

    TransactionType(CalculateSumOfQuantity instance) {
        sumImpl = instance;
    }

    public interface CalculateSumOfQuantity {
        BigDecimal calculateCurrentSumOfQuantity(Transaction transaction1, Transaction t2);
    }

    public static class DefaultSumQuantity implements CalculateSumOfQuantity {

        @Override
        public BigDecimal calculateCurrentSumOfQuantity(Transaction transaction1, Transaction t2) {
            return transaction1.getQuantity().add(t2.getQuantity());
        }
    }

}
