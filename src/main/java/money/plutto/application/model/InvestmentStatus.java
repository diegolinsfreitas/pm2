package money.plutto.application.model;

public enum InvestmentStatus {
    ACTIVE,
    INACTIVE
}
