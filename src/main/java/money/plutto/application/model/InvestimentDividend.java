package money.plutto.application.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;

import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class InvestimentDividend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Investment investment;

    private BigDecimal quantity;

    private DividendType type;

    @Columns(columns = {@Column(name = "value_per_unit_currency", length = 3), @Column(name = "value_per_unit_value", precision = 10, scale = 10)})
    @Type(type = "money")
    private MonetaryAmount valuePerUnit;

    @Columns(columns = {@Column(name = "tax_currency", length = 3), @Column(name = "tax_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount tax;

    @Columns(columns = {@Column(name = "total_received_currency", length = 3), @Column(name = "total_received_value", precision = 19, scale = 5)})
    @Type(type = "money")
    private MonetaryAmount totalReceived;

    private OffsetDateTime dateEx;

    private OffsetDateTime datePayment;
}
