package money.plutto.application.model;

import money.plutto.application.security.User;

public interface UserAware {
    void setUser(User user);
}
