package money.plutto.application;

import money.plutto.application.model.Asset;
import money.plutto.application.model.AssetType;
import money.plutto.application.model.Contribution;
import money.plutto.application.repository.AssetRepository;
import money.plutto.application.repository.AssetTypeRepository;
import money.plutto.application.repository.ContributionsRepository;
import money.plutto.application.security.Role;
import money.plutto.application.security.RoleRepository;
import money.plutto.application.security.UserRepository;
import lombok.AllArgsConstructor;
import money.plutto.application.security.User;
import org.javamoney.moneta.Money;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

@Component
@AllArgsConstructor
public class DataInitalizer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AssetTypeRepository assetTypeRepository;
    private final AssetRepository assetRepository;
    private final ContributionsRepository contributionsRepository;

    private final PasswordEncoder encoder;

    @Override
    public void run(String... args) throws Exception {
        if(assetTypeRepository.findAll().isEmpty()) {
            assetTypeRepository.save(AssetType.builder().name("STOCK").build());
            assetTypeRepository.save(AssetType.builder().name("AÇÕES").build());
            assetTypeRepository.save(AssetType.builder().name("FII").build());
            assetTypeRepository.save(AssetType.builder().name("REITS").build());
            assetTypeRepository.save(AssetType.builder().name("RENDA FIXA").build());
        }
        
        if (assetRepository.findAll().isEmpty()) {
            assetRepository.save(Asset.builder()
                    .symbol("ABEV3.SA")
                    .lastPrice(Money.of(19, "BRL"))
                    .assetType(
                            assetTypeRepository.findOne(Example.of(AssetType.builder().name("AÇÕES").build())).get())
                    .build());
        }
        
        if(userRepository.findAll().isEmpty()) {
            List<Role> roles = roleRepository.saveAll(Arrays.asList(new Role("USER"), new Role("INVESTOR")));
            User user = User.builder()
                    .email("diego.freitas@plutto.money")
                    .name("Diego")
                    .password(encoder.encode("123456"))
                    .roles(roles)
                    .isAccountNonExpired(false)
                    .isEnabled(true)
                    .isAccountNonLocked(false)
                    .isCredentialsNonExpired(false).build();
            userRepository.save(user);
        }

        if(contributionsRepository.findAll().isEmpty()) {
            contributionsRepository.save(Contribution.builder()
                    .date(LocalDate.now())
                    .amount(Money.of(2000, "BRL"))
                    .user(userRepository.findAll().get(0))
                    .build());
        }
    }
}
