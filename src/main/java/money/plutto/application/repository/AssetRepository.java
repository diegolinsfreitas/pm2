package money.plutto.application.repository;

import money.plutto.application.model.Asset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssetRepository extends JpaRepository<Asset, Long> {

    List<Asset> findAssetsBySymbolContains(String key);
    List<Asset> findAssetsBySymbolIsIn(String[] key);
}
