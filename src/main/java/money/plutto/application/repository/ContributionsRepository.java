package money.plutto.application.repository;

import money.plutto.application.model.Contribution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContributionsRepository extends JpaRepository<Contribution, Long> {

}
