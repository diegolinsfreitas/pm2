package money.plutto.application;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter
public class MonetaryAmountConverter implements AttributeConverter<MonetaryAmount, BigDecimal> {

    public static MonetaryAmount ZERO = Monetary.getDefaultAmountFactory().setNumber(0).setCurrency("BRL").create();

    @Override
    public BigDecimal convertToDatabaseColumn(MonetaryAmount attribute) {
        return attribute == null? BigDecimal.ZERO: BigDecimal.valueOf(attribute.getNumber().doubleValueExact());
    }

    @Override
    public MonetaryAmount convertToEntityAttribute(BigDecimal dbData) {
        return dbData == null? ZERO: Monetary.getDefaultAmountFactory().setNumber(dbData).setCurrency("BRL").create();
    }
}