package money.plutto.application;

import at.favre.lib.idmask.Config;
import at.favre.lib.idmask.IdMask;
import at.favre.lib.idmask.IdMasks;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.zalando.jackson.datatype.money.MoneyModule;

@SpringBootApplication(exclude = LiquibaseAutoConfiguration.class)//TODO exclude liquibase without explictly excluding it, maybe removinv some libs
@EnableFeignClients
@EnableScheduling
@EnableCaching
@Configuration
public class PortfolioManagerApplication {

	@Value("${id-mask.key}")
	private String idMaskKey;

	public static void main(String[] args) {
		SpringApplication.run(PortfolioManagerApplication.class, args);
	}

	@Bean
	public FlywayMigrationStrategy flywayMigrationStrategy() {
		return flyway -> {
			// do nothing
		};
	}


	@Bean
	@Profile("gcp")
	public IdMask<Long> providesIdMask(){
		return IdMasks.forLongIds(Config.builder(idMaskKey.getBytes()).build());
	}

	@Bean
	@Profile("!gcp")
	public IdMask<Long> providesDummyIdMask(){
		return new IdMask<>() {

			@Override
			public String mask(Long aLong) {
				return aLong.toString();
			}

			@Override
			public Long unmask(String s) {
				return Long.valueOf(s);
			}
		};
	}



	@Bean
	public ObjectMapper providesObjectMapper(){
		ObjectMapper objectMapper = new ObjectMapper()
				.registerModule(new MoneyModule()).registerModule(new JavaTimeModule())
				.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		return objectMapper;

	}

}
