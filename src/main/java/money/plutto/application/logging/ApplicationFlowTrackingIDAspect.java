package money.plutto.application.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * This class sets up the tracking id used in the logging for each new requests the app receives that starts a new
 * application flow
 * TODO add current user identification to de MDC Context?
 */
@Component
@Aspect
@Slf4j
public class ApplicationFlowTrackingIDAspect {

    @Pointcut("@annotation(money.plutto.application.logging.ApplicationFlowTrackingID)")
    public void annotatedMethod() {}

    @Pointcut("@within(money.plutto.application.logging.ApplicationFlowTrackingID)")
    public void annotatedClass() {}

    @Before("execution(* *(..)) && (annotatedMethod() || annotatedClass())")
    public void logMethodCall(JoinPoint jp) {
        String applicationFlowTrackingId = UUID.randomUUID().toString();
        MDC.put("ApplicationFlowTrackingID", applicationFlowTrackingId);
    }

    @After("execution(* *(..)) && (annotatedMethod() || annotatedClass())")
    public void clearMdc() {
        MDC.clear();
    }
}