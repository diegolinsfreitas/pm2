package money.plutto.application;

import money.plutto.application.security.idmask.ObfuscatedAnnotationFormatterFactory;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import java.util.concurrent.TimeUnit;

@Configuration
@AllArgsConstructor
public class WebConfig implements WebMvcConfigurer {

    private final ObfuscatedAnnotationFormatterFactory obfuscatedAnnotationFormatterFactory;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatterForFieldAnnotation(obfuscatedAnnotationFormatterFactory);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        WebContentInterceptor interceptor = new WebContentInterceptor();
        interceptor.addCacheMapping(CacheControl.maxAge(15, TimeUnit.MINUTES));//TODO not working, not applied in the request
        registry.addInterceptor(interceptor);
    }
}