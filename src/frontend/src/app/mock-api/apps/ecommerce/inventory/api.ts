import { Injectable } from '@angular/core';
import { assign, cloneDeep } from 'lodash-es';
import { FuseMockApiService, FuseMockApiUtils } from '@fuse/lib/mock-api';
import { brands as brandsData, categories as categoriesData, contributions as contributionsData, tags as tagsData, vendors as vendorsData } from 'app/mock-api/apps/contribution/Contribution/data';

@Injectable({
    providedIn: 'root'
})
export class ECommerceContributionMockApi
{
    private _categories: any[] = categoriesData;
    private _brands: any[] = brandsData;
    private _contributions: any[] = contributionsData;
    private _tags: any[] = tagsData;
    private _vendors: any[] = vendorsData;

    /**
     * Constructor
     */
    constructor(private _fuseMockApiService: FuseMockApiService)
    {
        // Register Mock API handlers
        this.registerHandlers();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register Mock API handlers
     */
    registerHandlers(): void
    {
        // -----------------------------------------------------------------------------------------------------
        // @ Categories - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/categories')
            .reply(() => [200, cloneDeep(this._categories)]);

        // -----------------------------------------------------------------------------------------------------
        // @ Brands - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/brands')
            .reply(() => [200, cloneDeep(this._brands)]);

        // -----------------------------------------------------------------------------------------------------
        // @ Contributions - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/contributions', 300)
            .reply(({request}) => {

                // Get available queries
                const search = request.params.get('search');
                const sort = request.params.get('sort') || 'name';
                const order = request.params.get('order') || 'asc';
                const page = parseInt(request.params.get('page') ?? '1', 10);
                const size = parseInt(request.params.get('size') ?? '10', 10);

                // Clone the contributions
                let contributions: any[] | null = cloneDeep(this._contributions);

                // Sort the contributions
                if ( sort === 'sku' || sort === 'name' || sort === 'active' )
                {
                    contributions.sort((a, b) => {
                        const fieldA = a[sort].toString().toUpperCase();
                        const fieldB = b[sort].toString().toUpperCase();
                        return order === 'asc' ? fieldA.localeCompare(fieldB) : fieldB.localeCompare(fieldA);
                    });
                }
                else
                {
                    contributions.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
                }

                // If search exists...
                if ( search )
                {
                    // Filter the contributions
                    contributions = contributions.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
                }

                // Paginate - Start
                const contributionsLength = contributions.length;

                // Calculate pagination details
                const begin = page * size;
                const end = Math.min((size * (page + 1)), contributionsLength);
                const lastPage = Math.max(Math.ceil(contributionsLength / size), 1);

                // Prepare the pagination object
                let pagination = {};

                // If the requested page number is bigger than
                // the last possible page number, return null for
                // contributions but also send the last possible page so
                // the app can navigate to there
                if ( page > lastPage )
                {
                    contributions = null;
                    pagination = {
                        lastPage
                    };
                }
                else
                {
                    // Paginate the results by size
                    contributions = contributions.slice(begin, end);

                    // Prepare the pagination mock-api
                    pagination = {
                        length    : contributionsLength,
                        size      : size,
                        page      : page,
                        lastPage  : lastPage,
                        startIndex: begin,
                        endIndex  : end - 1
                    };
                }

                // Return the response
                return [
                    200,
                    {
                        contributions,
                        pagination
                    }
                ];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Contribution - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/contribution')
            .reply(({request}) => {

                // Get the id from the params
                const id = request.params.get('id');

                // Clone the contributions
                const contributions = cloneDeep(this._contributions);

                // Find the contribution
                const contribution = contributions.find(item => item.id === id);

                // Return the response
                return [200, contribution];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Contribution - POST
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/apps/contribution/Contribution/contribution')
            .reply(() => {

                // Generate a new contribution
                const newContribution = {
                    id         : FuseMockApiUtils.guid(),
                    category   : '',
                    name       : 'A New Contribution',
                    description: '',
                    tags       : [],
                    sku        : '',
                    barcode    : '',
                    brand      : '',
                    vendor     : '',
                    stock      : '',
                    reserved   : '',
                    cost       : '',
                    basePrice  : '',
                    taxPercent : '',
                    price      : '',
                    weight     : '',
                    thumbnail  : '',
                    images     : [],
                    active     : false
                };

                // Unshift the new contribution
                this._contributions.unshift(newContribution);

                // Return the response
                return [200, newContribution];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Contribution - PATCH
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPatch('api/apps/contribution/Contribution/contribution')
            .reply(({request}) => {

                // Get the id and contribution
                const id = request.body.id;
                const contribution = cloneDeep(request.body.contribution);

                // Prepare the updated contribution
                let updatedContribution = null;

                // Find the contribution and update it
                this._contributions.forEach((item, index, contributions) => {

                    if ( item.id === id )
                    {
                        // Update the contribution
                        contributions[index] = assign({}, contributions[index], contribution);

                        // Store the updated contribution
                        updatedContribution = contributions[index];
                    }
                });

                // Return the response
                return [200, updatedContribution];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Contribution - DELETE
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onDelete('api/apps/contribution/Contribution/contribution')
            .reply(({request}) => {

                // Get the id
                const id = request.params.get('id');

                // Find the contribution and delete it
                this._contributions.forEach((item, index) => {

                    if ( item.id === id )
                    {
                        this._contributions.splice(index, 1);
                    }
                });

                // Return the response
                return [200, true];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Tags - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/tags')
            .reply(() => [200, cloneDeep(this._tags)]);

        // -----------------------------------------------------------------------------------------------------
        // @ Tags - POST
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPost('api/apps/contribution/Contribution/tag')
            .reply(({request}) => {

                // Get the tag
                const newTag = cloneDeep(request.body.tag);

                // Generate a new GUID
                newTag.id = FuseMockApiUtils.guid();

                // Unshift the new tag
                this._tags.unshift(newTag);

                // Return the response
                return [200, newTag];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Tags - PATCH
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onPatch('api/apps/contribution/Contribution/tag')
            .reply(({request}) => {

                // Get the id and tag
                const id = request.body.id;
                const tag = cloneDeep(request.body.tag);

                // Prepare the updated tag
                let updatedTag = null;

                // Find the tag and update it
                this._tags.forEach((item, index, tags) => {

                    if ( item.id === id )
                    {
                        // Update the tag
                        tags[index] = assign({}, tags[index], tag);

                        // Store the updated tag
                        updatedTag = tags[index];
                    }
                });

                // Return the response
                return [200, updatedTag];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Tag - DELETE
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onDelete('api/apps/contribution/Contribution/tag')
            .reply(({request}) => {

                // Get the id
                const id = request.params.get('id');

                // Find the tag and delete it
                this._tags.forEach((item, index) => {

                    if ( item.id === id )
                    {
                        this._tags.splice(index, 1);
                    }
                });

                // Get the contributions that have the tag
                const contributionsWithTag = this._contributions.filter(contribution => contribution.tags.indexOf(id) > -1);

                // Iterate through them and delete the tag
                contributionsWithTag.forEach((contribution) => {
                    contribution.tags.splice(contribution.tags.indexOf(id), 1);
                });

                // Return the response
                return [200, true];
            });

        // -----------------------------------------------------------------------------------------------------
        // @ Vendors - GET
        // -----------------------------------------------------------------------------------------------------
        this._fuseMockApiService
            .onGet('api/apps/contribution/Contribution/vendors')
            .reply(() => [200, cloneDeep(this._vendors)]);
    }
}
