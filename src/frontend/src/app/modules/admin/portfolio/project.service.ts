import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {environment} from "../../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class ProjectService
{
    private _data: BehaviorSubject<any> = new BehaviorSubject(null);
    private _portfolios: BehaviorSubject<any> = new BehaviorSubject(null);
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for data
     */
    get data$(): Observable<any>
    {
        return this._data.asObservable();
    }
    get portfolios$(): Observable<any>
    {
        return this._portfolios.asObservable();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get data
     */
    getData(): Observable<any>
    {
        return this._httpClient.get(`${environment.apiServer}/portfolios/`).pipe(
            tap((response: any) => {
                // this._data.next(response);
            })
        );

    }
    getPortfolios(): Observable<any>
    {
        return this._httpClient.get(`${environment.apiServer}/portfolios/`).pipe(
            tap((response: any) => {
                console.log("In_portfolioService_getPortfolios", response);
                this._portfolios.next(response);
            })
        );
    }
    addPortfolio(objectPassed): Observable<any>
    {
        return this._httpClient.post(`${environment.apiServer}/portfolios/`, objectPassed).pipe(
            tap((response: any) => {
                // this._portfolios.next(response);
                console.log("In_portfolioService_addPortfolio", response);
            })
        );
    }
}
