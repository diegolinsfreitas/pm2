import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Contribution} from 'app/modules/admin/contributions/contribution.types';
import {environment} from "../../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class ContributionService
{
    // Private
    /*private _brands: BehaviorSubject<ContributionBrand[] | null> = new BehaviorSubject(null);
    private _categories: BehaviorSubject<ContributionCategory[] | null> = new BehaviorSubject(null);
    private _pagination: BehaviorSubject<ContributionPagination | null> = new BehaviorSubject(null);
    private _tags: BehaviorSubject<ContributionTag[] | null> = new BehaviorSubject(null);
    private _vendors: BehaviorSubject<ContributionVendor[] | null> = new BehaviorSubject(null);*/

    private _contributions: BehaviorSubject<Contribution[] | null> = new BehaviorSubject([]);
    private _contribution: BehaviorSubject<Contribution | null> = new BehaviorSubject(null);



    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------



    /**
     * Getter for contribution
     */
    get contribution$(): Observable<Contribution>
    {
        return this._contribution.asObservable();
    }

    /**
     * Getter for contributions
     */
    get contributions$(): Observable<Contribution[]>
    {
        return this._contributions.asObservable();
    }



    /**
     * Get contributions
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getContributions(page: number = 0, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
        Observable<Contribution[]>
    {
        return this._httpClient.get<Contribution[]>(`${environment.apiServer}/contributions/`).pipe(
            tap((response:any) => {
                this._contributions.next(response);
            })
        );
    }

    /**
     * Get contribution by id
     */
    getContributionById(id: string): Observable<Contribution>
    {
        return this._contributions.pipe(
            take(1),
            map((contributions) => {

                // Find the contribution
                const contribution = contributions.find(item => item.id === id) || null;

                // Update the contribution
                this._contribution.next(contribution);

                // Return the contribution
                return contribution;
            }),
            switchMap((contribution) => {

                if ( !contribution )
                {
                    console.log('Could not found contribution with id of ' + id + '!');
                    return null;
                }

                return of(contribution);
            })
        );
    }

    /**
     * Create contribution
     */
    createContribution(): Observable<Contribution>
    {
        return this.contributions$.pipe(
            take(1),
            switchMap(contributions => this._httpClient.post<Contribution>(`${environment.apiServer}/contributions/`, {}).pipe(
                map((newContribution) => {

                    // Update the contributions with the new contribution
                    this._contributions.next([newContribution, ...contributions]);

                    // Return the new contribution
                    return newContribution;
                })
            ))
        );
    }

    /**
     * Update contribution
     *
     * @param id
     * @param contribution
     */
    updateContribution(id: string, contribution: Contribution): Observable<Contribution>
    {
        return this.contributions$.pipe(
            take(1),
            switchMap(contributions => this._httpClient.put<Contribution>(`${environment.apiServer}/contributions/${id}`, contribution).pipe(
                map((updatedContribution) => {

                    // Find the index of the updated contribution
                    const index = contributions.findIndex(item => item.id === id);

                    // Update the contribution
                    contributions[index] = updatedContribution;

                    // Update the contributions
                    this._contributions.next(contributions);

                    // Return the updated contribution
                    return updatedContribution;
                }),
                switchMap(updatedContribution => this.contribution$.pipe(
                    take(1),
                    filter(item => item && item.id === id),
                    tap(() => {

                        // Update the contribution if it's selected
                        this._contribution.next(updatedContribution);

                        // Return the updated contribution
                        return updatedContribution;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the contribution
     *
     * @param id
     */
    deleteContribution(id: string): Observable<boolean>
    {
        return this.contributions$.pipe(
            take(1),
            switchMap(contributions => this._httpClient.delete(`${environment.apiServer}/contributions/${id}`, ).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted contribution
                    const index = contributions.findIndex(item => item.id === id);

                    // Delete the contribution
                    contributions.splice(index, 1);

                    // Update the contributions
                    this._contributions.next(contributions);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
    /*

    getTags(): Observable<ContributionTag[]>
    {
        return this._httpClient.get<ContributionTag[]>('api/contributions/Contribution/tags').pipe(
            tap((tags) => {
                this._tags.next(tags);
            })
        );
    }


    createTag(tag: ContributionTag): Observable<ContributionTag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.post<ContributionTag>('api/contributions/Contribution/tag', {tag}).pipe(
                map((newTag) => {

                    // Update the tags with the new tag
                    this._tags.next([...tags, newTag]);

                    // Return new tag from observable
                    return newTag;
                })
            ))
        );
    }


    updateTag(id: string, tag: ContributionTag): Observable<ContributionTag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.patch<ContributionTag>('api/contributions/Contribution/tag', {
                id,
                tag
            }).pipe(
                map((updatedTag) => {

                    // Find the index of the updated tag
                    const index = tags.findIndex(item => item.id === id);

                    // Update the tag
                    tags[index] = updatedTag;

                    // Update the tags
                    this._tags.next(tags);

                    // Return the updated tag
                    return updatedTag;
                })
            ))
        );
    }


    deleteTag(id: string): Observable<boolean>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.delete('api/contributions/Contribution/tag', {params: {id}}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted tag
                    const index = tags.findIndex(item => item.id === id);

                    // Delete the tag
                    tags.splice(index, 1);

                    // Update the tags
                    this._tags.next(tags);

                    // Return the deleted status
                    return isDeleted;
                }),
                filter(isDeleted => isDeleted),
                switchMap(isDeleted => this.contributions$.pipe(
                    take(1),
                    map((contributions) => {

                        // Iterate through the contacts
                        contributions.forEach((contribution) => {

                            const tagIndex = contribution.tags.findIndex(tag => tag === id);

                            // If the contact has the tag, remove it
                            if ( tagIndex > -1 )
                            {
                                contribution.tags.splice(tagIndex, 1);
                            }
                        });

                        // Return the deleted status
                        return isDeleted;
                    })
                ))
            ))
        );
    }


    getVendors(): Observable<ContributionVendor[]>
    {
        return this._httpClient.get<ContributionVendor[]>('api/contributions/Contribution/vendors').pipe(
            tap((vendors) => {
                this._vendors.next(vendors);
            })
        );
    }


    uploadAvatar(id: string, avatar: File): Observable<Contact>
    {
        return this.contacts$.pipe(
            take(1),
            switchMap(contacts => this._httpClient.post<Contact>('api/apps/contacts/avatar', {
                id,
                avatar
            }, {
                headers: {
                    'Content-Type': avatar.type
                }
            }).pipe(
                map((updatedContact) => {

                    // Find the index of the updated contact
                    const index = contacts.findIndex(item => item.id === id);

                    // Update the contact
                    contacts[index] = updatedContact;

                    // Update the contacts
                    this._contacts.next(contacts);

                    // Return the updated contact
                    return updatedContact;
                }),
                switchMap(updatedContact => this.contact$.pipe(
                    take(1),
                    filter(item => item && item.id === id),
                    tap(() => {

                        // Update the contact if it's selected
                        this._contact.next(updatedContact);

                        // Return the updated contact
                        return updatedContact;
                    })
                ))
            ))
        );
    }*/
}
