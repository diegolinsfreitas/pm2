export interface Contribution
{
    id: string;
    amount : {
        amount: number;
        currency: string;
    };
    date: string;
}
/*
export interface ContributionPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
 */
/*
export interface ContributionCategory
{
    id: string;
    parentId: string;
    name: string;
    slug: string;
}

export interface ContributionBrand
{
    id: string;
    name: string;
    slug: string;
}

export interface ContributionTag
{
    id?: string;
    title?: string;
}

export interface ContributionVendor
{
    id: string;
    name: string;
    slug: string;
}
*/
