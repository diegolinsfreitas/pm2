import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ContributionService } from 'app/modules/admin/contributions/contribution.service';
import {Contribution} from 'app/modules/admin/contributions/contribution.types';
/*
@Injectable({
    providedIn: 'root'
})
export class ContributionBrandsResolver implements Resolve<any>
{

    constructor(private _ContributionService: ContributionService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ContributionBrand[]>
    {
        return this._ContributionService.getBrands();
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContributionCategoriesResolver implements Resolve<any>
{

    constructor(private _ContributionService: ContributionService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ContributionCategory[]>
    {
        return this._ContributionService.getCategories();
    }
}
*/
@Injectable({
    providedIn: 'root'
})
export class ContributionResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _ContributionService: ContributionService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Contribution | null>
    {
        if(!route.paramMap.get('id')){
            return null;
        }
        return this._ContributionService.getContributionById(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested contribution is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContributionListResolver implements Resolve<any>
{

    constructor(private _ContributionService: ContributionService)
    {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Contribution[]>
    {
        return this._ContributionService.getContributions();
    }
}
/*
@Injectable({
    providedIn: 'root'
})
export class ContributionTagsResolver implements Resolve<any>
{

    constructor(private _ContributionService: ContributionService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ContributionTag[]>
    {
        return this._ContributionService.getTags();
    }
}

@Injectable({
    providedIn: 'root'
})
export class ContributionVendorsResolver implements Resolve<any>
{

    constructor(private _ContributionService: ContributionService)
    {
    }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ContributionVendor[]>
    {
        return this._ContributionService.getVendors();
    }
}
*/
