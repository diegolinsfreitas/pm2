import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { Contribution } from 'app/modules/admin/contributions/contribution.types';
import { ContributionService } from 'app/modules/admin/contributions/contribution.service';

@Component({
    selector       : 'contribution-list',
    templateUrl    : './contribution.component.html',
    styles         : [
        /* language=SCSS */
        `
            .contribution-grid {
                grid-template-columns: 48px auto 40px;

                @screen sm {
                    grid-template-columns: 48px auto 112px 72px;
                }

                @screen md {
                    grid-template-columns: 48px 112px auto 112px 72px;
                }

                @screen lg {
                    grid-template-columns: 48px 112px auto 112px 96px 96px 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class ContributionListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    contributions$: Observable<Contribution[]>;


    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    searchInputControl: FormControl = new FormControl();
    selectedContribution: Contribution | null = null;
    selectedContributionForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _ContributionService: ContributionService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected contribution form
        this.selectedContributionForm = this._formBuilder.group({
            id      : [''],
            amount  : this._formBuilder.group({
                amount: ['', [Validators.required]],
                currency: ['BRL']
            }),
            date : ['', [Validators.required]]
        });


        // Get the contributions
        this.contributions$ = this._ContributionService.contributions$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._ContributionService.getContributions(0, 10, 'name', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'name',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get contributions if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._ContributionService.getContributions(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle contribution details
     *
     * @param contributionId
     */
    toggleDetails(contributionId: string): void
    {
        // If the contribution is already selected...
        if ( this.selectedContribution && this.selectedContribution.id === contributionId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the contribution by id
        this._ContributionService.getContributionById(contributionId)
            .subscribe((contribution) => {

                // Set the selected contribution
                this.selectedContribution = contribution;

                // Fill the form
                this.selectedContributionForm.patchValue(contribution);

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedContribution = null;
    }

    /**
     * Create contribution
     * TODO change creation logic! do not post when click add button..
     * TODO add datePicker component to contributions
     */
    createContribution(): void
    {
        // Create the contribution

        this._ContributionService.createContribution().subscribe((newContribution) => {

            // Go to new contribution
            this.selectedContribution = newContribution;

            // Fill the form
            this.selectedContributionForm.patchValue(newContribution);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected contribution using the form data
     */
    updateSelectedContribution(): void
    {
        // Get the contribution object
        const contribution = this.selectedContributionForm.getRawValue();


        // Update the contribution on the server
        this._ContributionService.updateContribution(contribution.id, contribution).subscribe(() => {
            // Show a success message
            this.showFlashMessage('success');
        });
    }

    /**
     * Delete the selected contribution using the form data
     */
    deleteSelectedContribution(): void
    {
        // Get the contribution object
        const contribution = this.selectedContributionForm.getRawValue();

        // Delete the contribution on the server
        this._ContributionService.deleteContribution(contribution.id).subscribe(() => {

            // Close the details
            this.closeDetails();
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.id || index;
    }
}
