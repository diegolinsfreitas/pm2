import { Route } from '@angular/router';
import { ContributionListComponent } from 'app/modules/admin/contributions/list/contribution.component';
import {
    ContributionListResolver,
    ContributionResolver
} from 'app/modules/admin/contributions/contribution.resolvers';

export const contributionRoutes: Route[] = [

    {
        path     : '',
        component: ContributionListComponent,
        resolve  : {
            contributions   : ContributionListResolver,
            contribution    : ContributionResolver
        }
    }
/*{
    path     : 'contribusdfsdtions',
    component: ContributionComponent,
    children : [
        {
            path     : '',
            component: ContactsListComponent,
            resolve  : {
                tasks    : ContactsResolver,
                countries: ContactsCountriesResolver
            },
            children : [
                {
                    path         : ':id',
                    component    : ContactsDetailsComponent,
                    resolve      : {
                        task     : ContactsContactResolver,
                        countries: ContactsCountriesResolver
                    },
                    canDeactivate: [CanDeactivateContactsDetails]
                }
            ]
        }
    ]
    }*/
];
