package money.plutto.application;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import money.plutto.application.model.Asset;
import money.plutto.application.service.AssetService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class
})
@Slf4j
public class AssetServiceTest {


    @Autowired
    private AssetService assetService;

    @LocalServerPort
    private int port;

    private URL base;

    private TestRestTemplate template;

    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    @BeforeEach
    public void setUp() throws MalformedURLException, URISyntaxException {
        this.base = new URL("http://localhost:" + port + "/");
        this.template = new TestRestTemplate(restTemplateBuilder
                .rootUri(base.toURI().toString())); // I needed cookie support in this particular test, you may not have this need

    }

    @Test
    @DatabaseSetup(value = "/dataset/portfolio.xml", type = DatabaseOperation.CLEAN_INSERT)
    public void shouldGetAssetData() {
        List<Asset> assets = assetService.findAssetByName("WEGE");
        Assertions.assertEquals("WEGE3", assets.stream().findFirst().get().getSymbol() );
    }

    @Test
    @DatabaseSetup( type = DatabaseOperation.CLEAN_INSERT)
    public void shouldDownloadAssetDataWhenMissing() {
        List<Asset> assets = assetService.findAssetByName("WEGE3.SA");
        Assertions.assertEquals("WEGE3.SA", assets.stream().findFirst().get().getSymbol() );
    }



}
