package money.plutto.application;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import money.plutto.application.model.Investment;
import money.plutto.application.model.Portfolio;
import money.plutto.application.model.Transaction;
import money.plutto.application.model.TransactionType;
import money.plutto.application.repository.InvestmentRepository;
import money.plutto.application.repository.PortfolioRepository;
import money.plutto.application.service.PortfolioService;
import money.plutto.application.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class
})
@Slf4j
public class PortfolioServiceTest {

    @Autowired
    private PortfolioRepository portfolioRepository;

    @Autowired
    private InvestmentRepository investmentRepository;

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private TransactionService transactionService;

    @LocalServerPort
    private int port;

    private URL base;

    private TestRestTemplate template;

    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    @BeforeEach
    public void setUp() throws MalformedURLException, URISyntaxException {
        this.base = new URL("http://localhost:" + port + "/");
        this.template = new TestRestTemplate(restTemplateBuilder
                .rootUri(base.toURI().toString())); // I needed cookie support in this particular test, you may not have this need

    }

    @DatabaseTearDown(value = "/dataset/portfolio.xml", type = DatabaseOperation.TRUNCATE_TABLE)
    public void dbTearDown() {

    }

    @Test
    @DatabaseSetup(value = "/dataset/portfolio.xml", type = DatabaseOperation.CLEAN_INSERT)
    public void shouldAddNewInvestmentToExistingPortfolio() {
        portfolioService.addInvestmentToPortfolio(1L, 1L);

        Investment investment = portfolioRepository.findById(1L).get().getInvestments().stream().filter(a -> a.getAsset().getSymbol().equals("ABEV3")).findFirst().get();
        Assertions.assertEquals("ABEV3", investment.getAsset().getSymbol() );
        Assertions.assertEquals(BigDecimal.ZERO, investment.getInvestimentConsolidated().getQuantity() );
    }

    @Test
    @DatabaseSetup(value = "/dataset/portfolio.xml", type = DatabaseOperation.CLEAN_INSERT)
    public void shouldNotAddExistingInvestmentToPortfolio() {
        log.error(Assertions.assertThrows(RuntimeException.class, () ->{
            portfolioService.addInvestmentToPortfolio(1L, 2L);//Add wege3
        }).getMessage());
    }

    @Test
    @DatabaseSetup(value = "/dataset/portfolio.xml", type = DatabaseOperation.CLEAN_INSERT)
    public void shouldAddTransactionForInvestmentInPortfolio() {
        Transaction transactionDTO = Transaction.builder()
                .quantity(BigDecimal.TEN)
                .totalTransaction(Money.of(300,"BRL"))
                .type(TransactionType.BUY).build();
        transactionService.addTransaction(99L, transactionDTO);

        Portfolio portfolio = portfolioRepository.findById(1L).get();
        portfolio.calculateCurrentValueAndPositions();
        Assertions.assertEquals(Money.of(340,"BRL"),portfolio.getCurrentValue());
        Investment investment = portfolio.getInvestments().stream().findFirst().get();
        Assertions.assertEquals(Money.of(340,"BRL"),investment.getInvestimentConsolidated().getCurrentValue());
        Assertions.assertEquals(BigDecimal.TEN.setScale(2),investment.getInvestimentConsolidated().getQuantity());
        Assertions.assertEquals(BigDecimal.ONE.setScale(1),investment.getInvestimentConsolidated().getPercentagePosition());
        Assertions.assertEquals(BigDecimal.ZERO.doubleValue(),investment.getMonetaryDiff().getNumber().doubleValue());
        Assertions.assertEquals(BigDecimal.ZERO,investment.getPercentageDiff());

    }

    @Test
    @DatabaseSetup(value = "/dataset/portfolio.xml", type = DatabaseOperation.CLEAN_INSERT)
    public void shouldAddTransactionForTwoInvestmentInPortfolio() {
        Investment wege3 = investmentRepository.findById(99L).get();
        wege3.setPercentageTarget(BigDecimal.valueOf(0.4));
        investmentRepository.save(wege3);

        Transaction transactionDTO = Transaction.builder()
                .quantity(BigDecimal.TEN)
                .totalTransaction(Money.of(300, "BRL"))
                .type(TransactionType.BUY).build();
        transactionService.addTransaction(99L, transactionDTO);

        Investment abev3 = portfolioService.addInvestmentToPortfolio(1L, 1L);
        abev3.setPercentageTarget(BigDecimal.valueOf(0.6));
        investmentRepository.save(abev3);

        Transaction transactionDTO2 = Transaction.builder()
                .quantity(BigDecimal.TEN)
                .totalTransaction(Money.of(190, "BRL"))
                .type(TransactionType.BUY).build();
        transactionService.addTransaction(abev3.getId(), transactionDTO2);


        Portfolio portfolio = portfolioRepository.findById(1L).get();
        portfolio.calculateCurrentValueAndPositions();
        Assertions.assertEquals(Money.of(539,"BRL"),portfolio.getCurrentValue());

        Investment investment = portfolio.getInvestments().stream().filter(a -> a.getAsset().getSymbol().equals("WEGE3")).findFirst().get();
        Assertions.assertEquals(Money.of(340,"BRL"),investment.getInvestimentConsolidated().getCurrentValue());
        Assertions.assertEquals(BigDecimal.TEN.setScale(2),investment.getInvestimentConsolidated().getQuantity());
        Assertions.assertEquals(BigDecimal.valueOf(0.6307).setScale(2, RoundingMode.CEILING),investment.getInvestimentConsolidated().getPercentagePosition().setScale(2,RoundingMode.CEILING));
        Assertions.assertEquals(BigDecimal.valueOf(124.40).setScale(2),BigDecimal.valueOf(investment.getMonetaryDiff().getNumber().doubleValue()).setScale(2,RoundingMode.CEILING));
        Assertions.assertEquals(BigDecimal.valueOf(0.230797774).setScale(2,RoundingMode.CEILING),investment.getPercentageDiff().setScale(2,RoundingMode.CEILING));

        Investment investment2 = portfolio.getInvestments().stream().filter(a -> a.getAsset().getSymbol().equals("ABEV3")).findFirst().get();
        Assertions.assertEquals(Money.of(199,"BRL"),investment2.getInvestimentConsolidated().getCurrentValue());
        Assertions.assertEquals(BigDecimal.TEN.setScale(2),investment2.getInvestimentConsolidated().getQuantity());
        Assertions.assertEquals(BigDecimal.valueOf(0.37),investment2.getInvestimentConsolidated().getPercentagePosition().setScale(2, RoundingMode.CEILING));
        Assertions.assertEquals(BigDecimal.valueOf(-124.40).setScale(2),BigDecimal.valueOf(investment2.getMonetaryDiff().getNumber().doubleValue()).setScale(2,RoundingMode.FLOOR));
        Assertions.assertEquals(BigDecimal.valueOf(-0.230797774).setScale(2,RoundingMode.CEILING),investment2.getPercentageDiff().setScale(2,RoundingMode.CEILING));

    }

    @Test
    public void shouldCalculateCumulativeSumOfInvestedMoneyAndQuantityGivenItsTransactionHistoryWithOnlyBuyTransactions() {
        //throw new NotImplementedException();
    }

    @Test
    public void shouldCalculateCumulativeSumOfInvestedMoneyForEachInvestimentGivenItsTransactionHistoryWithBuyAndSellTransactions() {
        //given investment with 600 stock and a cumulative invested amount of 22000
        //and sell 100

        //then the cumulative invested amount 18333.333
        //calc: 22000 - ((22000/600)*100)
        //throw new NotImplementedException();
    }

    @Test
    public void shouldCalculateSummarySplitTransaction() {
        //throw new NotImplementedException();
    }

    @Test
    public void shouldCalculateSummaryGroupingTransaction() {
        //throw new NotImplementedException();
    }

    @Test
    public void shouldCalculateSummaryStockBonusTransaction() {
        //throw new NotImplementedException();
    }

    @Test
    public void shouldCalculateDividendsGivenTransactionHistory() {
        //throw new NotImplementedException();
    }



}
