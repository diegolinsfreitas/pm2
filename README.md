Setup local db

Install docker (dockek.io version)

docker run --name=pluttodb -e MYSQL_DATABASE=pluttodb -e MYSQL_USER=plutto -e MYSQL_PASSWORD=plutto  -p 3306:3306  -d mysql/mysql-server:5.7

run locally using only the spring profile "local"
and do not use the maven profile cloud-gcp

Archtecture:

It is a Monotithic App with the frontend and the backend deployed in different services on gcp

We use principles of Domain driven design, wich means we try to implement the domain logic inside the domain model (classes annotated with @Entity) whenever possible with some exceptions.

Apply Design patterns like strategy, decorator to avoid lots of if/else statments.

The database is the single source of truth!!! The backend always lookup from the database in order to provide data to the frontend!!! We implement dataAdapters that runs asychronously
to pull latest data from many sources.
